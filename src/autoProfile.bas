Attribute VB_Name = "autoProfile"
Option Explicit


'
' Not for commercial use except by explicit permission
' bruce@mcpher.com
' http://sites.mcpher.com/share/Home/excelquirks/proctimer
'
' These procedures access and update your VBA modules and should be used with care
' To be able to use them
' - you need to allow access the project model. under tools/macros/security
' - you also have to add another reference -In the VB editor go to tools, references
' and check something called 'microsoft visual basic for applications extensibilty'.
'

Sub apProfileModules(vj As capvProj)

    Dim vm As capvModule, vp As capvProc
    Dim n As Long
    n = 0


    ' make sure we have a manager
    
    For Each vm In vj.apvModules
        For Each vp In vm.apvProcs
            If vp.Selected Then
                n = n + 1
            End If
        Next vp
    Next vm
    

    
    If n = 0 Then
        MsgBox "No procedures selected for autoprofiling"
    End If
    
    ' get rid of previous autoprofile session & make new
    
    If vj.Manager Is Nothing Then
        MsgBox "Need a manager module "
    Else
        If n > 0 Then
            Call apRemoveProfiling(vj)
            Call apInsertProfiling(vj)
        End If
    End If
End Sub

Private Sub apInsertProfiling(vj As capvProj)
'
' set up new auto profiling
'
    Dim apvm As capvModule, apvp As capvProc
'
' set up public variable
    Set apvp = vj.Manager


' do the reporting
    For Each apvm In vj.apvModules
        With apvm.vCom.CodeModule
            For Each apvp In apvm.apvProcs
                If apvp.Selected Then
                    .InsertLines apAfterFirstCodeLine(apvp), apInsertStart(apvp)
                    .InsertLines apLastCodeLine(apvp), apInsertFinish(apvp)
                End If
                    
            Next apvp
        End With
    Next apvm
    
    Set apvp = vj.Manager
    
' manager stuff

    With apvp.Parent.vCom.CodeModule
        .InsertLines apAfterFirstCodeLine(apvp), apInsertStartProfiler(apvp)
        .InsertLines apAfterFirstCodeLine(apvp), apInsertConstruct(apvp)
        
        .InsertLines apLastCodeLine(apvp), apInsertFinishProfiler(apvp)
        .InsertLines apLastCodeLine(apvp), apInsertResults(apvp)
        .InsertLines apLastCodeLine(apvp), apInsertDestroy(apvp)
    End With

  
End Sub
Function apgetProjects() As Collection
    Dim wb As Workbook
    Dim co As Collection
    Dim apvj As capvProj
    Set co = New Collection
    For Each wb In Workbooks
        Set apvj = New capvProj
        apvj.Init wb
        co.Add apvj, apvj.NameandParent

    Next wb
    Set apgetProjects = co
End Function
Private Function apBeforeFirstProc(apvm As capvModule) As Long

' find line after first line of code
'
    Dim s As String, lEnd As Long, lStart As Long
    With apvm.vCom.CodeModule
        lStart = .CountOfDeclarationLines
    End With
    apBeforeFirstProc = lStart + 1
End Function
Private Function apLastCodeLine(apvp As capvProc) As Long
'
' find line of start of last line of code
'
    Dim s As String, lEnd As Long, lStart As Long, p As String
    With apvp.Parent.vCom.CodeModule
        lStart = .ProcBodyLine(apvp.Name, apvp.ProcKind)
        lEnd = .ProcCountLines(apvp.Name, apvp.ProcKind) + .ProcStartLine(apvp.Name, apvp.ProcKind) - 1
        ' get back to last non blank line
        
        While Len(Trim(.Lines(lEnd, 1))) = 0 And lEnd > lStart
            lEnd = lEnd - 1
        Wend

        'skip where previous line ends in continuation
        While Right(Trim(.Lines(lEnd - 1, 1)), 1) = "_" And lEnd > lStart
            lEnd = lEnd - 1
        Wend
        Debug.Assert lEnd > lStart
    End With
    apLastCodeLine = lEnd
End Function
Private Function apAfterFirstCodeLine(apvp As capvProc) As Long
'
' find line after first line of code
'
    Dim s As String, lEnd As Long, lStart As Long
    With apvp.Parent.vCom.CodeModule
        lStart = .ProcBodyLine(apvp.Name, apvp.ProcKind)
        lEnd = .ProcCountLines(apvp.Name, apvp.ProcKind) + .ProcStartLine(apvp.Name, apvp.ProcKind) - 1
        s = Trim(.Lines(lStart, 1))
        While (Right(s, 1) = "_") And lEnd > lStart
            lStart = lStart + 1
            s = Trim(.Lines(lStart, 1))
        Wend
    End With
    apAfterFirstCodeLine = lStart + 1
End Function
 Sub apRemoveProfiling(vj As capvProj)
'
' get rid of all existing automatic profiling
'
    Dim apvm As capvModule, apvp As capvProc
    Dim lStart As Long, lEnd As Long, cStart As Long, cEnd As Long, s As String

    
    For Each apvm In vj.apvModules
        With apvm.vCom.CodeModule


            For Each apvp In apvm.apvProcs
                lStart = apAfterFirstCodeLine(apvp)
                lEnd = apLastCodeLine(apvp)
                cStart = 1
                cEnd = 255

                While .Find(vj.Stub, lStart, cStart, lEnd, cEnd)
                
                    .DeleteLines lStart, lEnd - lStart + 1
                    lStart = apAfterFirstCodeLine(apvp)
                    lEnd = apLastCodeLine(apvp)
                    cStart = 1
                    cEnd = 255

                Wend
                
            Next apvp
            
            lStart = 1
            cStart = 1
            cEnd = 255
            lEnd = apBeforeFirstProc(apvm) - 1
            While .Find(vj.Stub, lStart, cStart, lEnd, cEnd)
            
                .DeleteLines lStart, lEnd - lStart + 1
                lStart = 1
                cStart = 1
                cEnd = 255
                lEnd = apBeforeFirstProc(apvm) - 1
            Wend
            
        End With
    Next apvm

End Sub

Sub apGetProcsWithProfiling(vj As capvProj)
'
' find which procs have profiling
'
    Dim apvm As capvModule, apvp As capvProc
    Dim s As String
    Dim lStart As Long, lEnd As Long, cStart As Long, cEnd As Long

    Set vj.Manager = Nothing
    Set vj.ResultsRange = Nothing
    
    For Each apvm In vj.apvModules
        With apvm.vCom.CodeModule
            For Each apvp In apvm.apvProcs
            
                lStart = apAfterFirstCodeLine(apvp)
                lEnd = apLastCodeLine(apvp)
                cStart = 1
                cEnd = 255
                apvp.Selected = False

                If .Find(apStubMarkSelected(apvp), lStart, cStart, lEnd, cEnd) Then
                    apvp.Selected = True
                End If
                
                lStart = apAfterFirstCodeLine(apvp)
                lEnd = apLastCodeLine(apvp)
                cStart = 1
                cEnd = 255
                
                If .Find(apStubMarkManager(apvp), lStart, cStart, lEnd, cEnd) Then
                    Set vj.Manager = apvp
                End If
                
                lStart = apAfterFirstCodeLine(apvp)
                lEnd = apLastCodeLine(apvp)
                cStart = 1
                cEnd = 255
                
                If .Find(apStubMarkRange(apvp), lStart, cStart, lEnd, cEnd) Then
                     s = .Lines(lStart, 1)
                     If s <> "" Then
                        Set vj.ResultsRange = Range(apGetRange(s))
                     End If
                End If
                
            Next apvp
        End With
    Next apvm

End Sub
 Sub apGetProcList(apvm As capvModule)
'
' get every proc in a module
'

    Dim lStart As Long, pname As String
    Dim n As Long
    Dim cm As CodeModule
    Dim pk As vbext_prockind
    Dim apvp As capvProc
    
    Set cm = apvm.vCom.CodeModule
    
    n = 0
    lStart = cm.CountOfDeclarationLines + 1
    While lStart <= cm.CountOfLines
        pname = cm.ProcOfLine(lStart, pk)
        Set apvp = New capvProc
        Call apvp.Init(apvm, pname, pk)
        lStart = cm.ProcStartLine(pname, pk) + cm.ProcCountLines(pname, pk)
    Wend


End Sub

Public Sub apGetmoduleList(vj As capvProj)
    Dim vList As Collection
    
    Dim v As VBComponent, vs As VBComponents, wb As Workbook
    Dim bInc As Boolean, n As Long

    Set vs = vj.wBook.VBProject.VBComponents
    Set vList = New Collection
    Dim apm As capvModule
    
    n = 0
    For Each v In vs

          Set apm = New capvModule
          Call apm.Init(v, vj)
          n = n + 1
          vj.apvModules.Add apm, LCase(Trim(apm.Name))

    Next v
    
End Sub

Private Function isInList(target As String, lst As Variant, Optional bExact As Boolean = False) As Long
    Dim n As Long, s As String, t As String
    
    isInList = -1
    
    If IsMissing(lst) Or IsEmpty(lst) Then
        Exit Function
    End If
    Debug.Assert LBound(lst) > -1

    If bExact Then
        t = target
    Else
        t = LCase(Trim(target))
    End If
    For n = LBound(lst) To UBound(lst) Step 1
        s = lst(n)
        If bExact Then
            s = lst(n)
        Else
            s = LCase(Trim(lst(n)))
        End If
        If s = t Then
            isInList = n
            Exit Function
        End If
    Next n
    
End Function
Public Function isinCollection(co As Collection, v As Variant) As Boolean
    Dim o As Variant
    On Error GoTo handle
    Set o = co(v)
    isinCollection = True
    Exit Function
handle:
    isinCollection = False
    
End Function
 
Private Function apInsertStartProfiler(vp As capvProc) As String
    apInsertStartProfiler = aPre() & apHandler(vp, "StartProfiler") & _
                " " & apQ(vp.Stub) & aPre() & apCommentsManager(vp)


End Function
Private Function apInsertConstruct(vp As capvProc) As String
    apInsertConstruct = aPre() & "call " & thisProject() & "apConstruct" & _
                apB(apQ(vp.Stub)) & aPre() & apCommentsManager(vp)

End Function
Private Function apInsertResults(vp As capvProc) As String
    apInsertResults = aPre() & apHandler(vp, "Results") & _
                aPre() & "range" & apB(apQ(vp.ResultsAddress)) & apCommentsRange(vp)
End Function
Private Function apInsertFinishProfiler(vp As capvProc) As String
    apInsertFinishProfiler = aPre() & apHandler(vp, "FinishProfiler") & _
                aPre() & apCommentsManager(vp)
    
End Function
Private Function apInsertDestroy(vp As capvProc) As String
       apInsertDestroy = aPre() & apHandler(vp, "DestroyTimers") & _
                aPre() & apCommentsManager(vp)
    
End Function

Private Function apInsertStart(vp As capvProc) As String
    apInsertStart = aPre() & apHandler(vp, "start") & _
                " " & apC(apQ(vp.NameandPK), apQ(vp.NameandParentandPK)) & aPre() & apComments(vp)

End Function
Private Function apInsertFinish(vp As capvProc) As String
    apInsertFinish = aPre() & apHandler(vp, "Finish") & _
                " " & apQ(vp.NameandPK) & aPre() & apComments(vp)
    
End Function
Private Function aPre() As String
    aPre = "    "
End Function

Private Function apQ(s As String) As String
    apQ = """" & s & """"
End Function
Private Function apB(s As String) As String
    apB = "(" & s & ")"
End Function
Private Function apC(s1 As String, s2 As String) As String
    apC = s1 & "," & s2
End Function
Private Function apID(vp As capvProc) As String
    apID = CStr(vp.Parent.Parent.id)
End Function
Private Function apComments(vp As capvProc) As String
    apComments = apStubMarkSelected(vp) & apGenComments()
End Function
Private Function apCommentsManager(vp As capvProc) As String
    apCommentsManager = apStubMarkManager(vp) & apGenComments()
End Function
Private Function apCommentsRange(vp As capvProc) As String
    apCommentsRange = apstubmarkRangeAdd(vp) & apGenComments()
End Function
Private Function apGenComments() As String
    apGenComments = ":" & procProfilerVersion & " on " & CStr(Date) & " at " & CStr(Time)
End Function
Private Function apStubMark(vp As capvProc) As String
    apStubMark = "'" & vp.Stub & " :apr"
End Function
Private Function apStubMarkManager(vp As capvProc) As String
    apStubMarkManager = apStubMark(vp) & ":mgr"
End Function
Private Function apStubMarkSelected(vp As capvProc) As String
    apStubMarkSelected = apStubMark(vp) & ":slc"
End Function
Private Function apStubMarkRange(vp As capvProc) As String
    apStubMarkRange = apStubMarkManager(vp) & ":rng"
End Function
Private Function apstubmarkRangeAdd(vp As capvProc)
    apstubmarkRangeAdd = apStubMarkRange(vp) & "|" & vp.Parent.Parent.ResultsAddress & "|"
End Function
Private Function apGetRange(sin As String) As String
    Dim s As String
    Dim a As Variant
    a = Split(sin, "|")
    apGetRange = a(1)
End Function
Public Function findwithNameModulePk(sname As String, vj As capvProj) As capvProc
    Dim vm As capvModule, vp As capvProc
    For Each vm In vj.apvModules
        If isinCollection(vm.apvProcs, sname) Then
            Set findwithNameModulePk = vm.apvProcs(sname)
            Exit Function
        End If
    Next vm
    Set findwithNameModulePk = Nothing
End Function
Private Function thisProject() As String
    thisProject = "cpProfiler."
End Function
Private Function apHandler(vp As capvProc, act As String) As String
    apHandler = thisProject() & vp.Stub & "." & act
End Function
