VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "cProcTimer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' note that you will need to add a reference to cProfiler to access the classes here
' Not for commercial use except by explicit permission
' bruce@mcpher.com
' http://sites.mcpher.com/share/Home/excelquirks/proctimer
'
' Acknowledgement for the microtimer procedures used here to
' thanks to Charles Wheeler - http://www.decisionmodels.com/
Private Declare Function getFrequency Lib "kernel32" _
Alias "QueryPerformanceFrequency" (cyFrequency As Currency) As Long
Private Declare Function getTickCount Lib "kernel32" _
Alias "QueryPerformanceCounter" (cyTickCount As Currency) As Long

Const defaultTimerPlaces = 5
Private lName As String         ' name of section being timed
Private lProcName As String     ' optional procedure name
Private lAdded As Date          ' time added to collection
Private lsTime As Double        ' microtimer started at last pause/start
Private lpTime As Double        ' microtimer passed so far this session
Private lIterations As Long     ' number of times called
Private ltTime As Double        ' total time for all iterations
Private lMaxTime As Double      ' max observation
Private lMinTime As Double      ' min observation
Private lFirstStart As Date     ' start of first iteration
Private lLastFinish As Date     ' finish of last iteration
Private lisOpen As Boolean      ' true while between start/finish
Private lLevel As ptLevels      ' level/depth of section
Private lActive As Boolean      ' whether its active in this session
Private lPaused As Boolean      ' are we currently paused
Public Property Get isOpen() As Boolean
    isOpen = lisOpen
End Property
Public Property Get Level() As ptLevels
    Level = lLevel
End Property
Public Property Let Level(pLevel As ptLevels)
    lLevel = pLevel
End Property
Public Property Get Active() As Boolean
    Active = lActive
End Property
Public Property Let Active(pActive As Boolean)
    lActive = pActive
End Property
Public Property Get Name() As String
    Name = lName
End Property
Public Property Let Name(sname As String)
    lName = sname
End Property
Public Property Get LastTime() As Date
    LastTime = lLastFinish
End Property
Public Property Get FirstTime() As Date
    FirstTime = lFirstStart
End Property
Public Property Get Added() As Date
    Added = lAdded
End Property
Public Property Get procName() As String
    procName = lProcName
End Property
Public Property Let procName(pname As String)
    lProcName = pname
End Property
Public Property Get Iterations() As Long
    Iterations = lIterations
End Property
Public Property Get TotalTime(Optional accuracy As Long = defaultTimerPlaces) As Double
    TotalTime = Round(ltTime, accuracy)
End Property
Public Property Get MeanTime(Optional accuracy As Long = defaultTimerPlaces) As Double
    MeanTime = Round(ltTime / lIterations, accuracy)
End Property
Public Property Get MaxTime(Optional accuracy As Long = defaultTimerPlaces) As Double
    MaxTime = Round(lMaxTime, accuracy)
End Property
Public Property Get MinTime(Optional accuracy As Long = defaultTimerPlaces) As Double
    MinTime = Round(lMinTime, accuracy)
End Property
Public Property Get soFar() As Double
    If lActive Then
        If lisOpen Then
            If lPaused Then
               soFar = lpTime
            Else
               soFar = cMicroTimer() - lsTime + lpTime
            End If
        Else
            soFar = 0
        End If
    Else
        soFar = 0
    End If
    
End Property

Public Sub Reset()

    lsTime = 0
    lIterations = 0
    lpTime = 0
    lMaxTime = -1
    lMinTime = -1
    lisOpen = False
    ltTime = 0
    lAdded = Now()
    lFirstStart = lAdded

End Sub
Public Sub Start()
    ' called when timing iteration starts
    Dim v As Variant
    If lActive Then
        lisOpen = True
        lPaused = False
        lsTime = cMicroTimer()
    End If
End Sub
Public Sub Pause()
    ' stop timing temporarily, but dont close off itertaion
    If lActive Then
        If Not lPaused Then
            lpTime = cMicroTimer() - lsTime + lpTime
            lPaused = True
        End If
    End If
End Sub
Public Sub Finish()
    ' stop timing and close off iteration
    If lActive Then
        Call Pause
        lIterations = lIterations + 1
        ltTime = ltTime + lpTime
        If lpTime > lMaxTime Or lMaxTime < 0 Then
            lMaxTime = lpTime
        End If
        If lpTime < lMinTime Or lMinTime < 0 Then
            lMinTime = lpTime
        End If
        lpTime = 0
        lLastFinish = Now()
        lisOpen = False
    End If
End Sub
Private Function cMicroTimer() As Double
' Returns seconds.
    Dim cyTicks1 As Currency
    Static cyFrequency As Currency
    cMicroTimer = 0
' Get frequency.
    If cyFrequency = 0 Then getFrequency cyFrequency
' Get ticks.
    getTickCount cyTicks1
' Seconds
    If cyFrequency Then cMicroTimer = cyTicks1 / cyFrequency
End Function

