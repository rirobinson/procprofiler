Attribute VB_Name = "apPublic"
Option Explicit
Public apProfiler_1 As cProcProfiler
Public Const procProfilerVersion = "0.2"

'
' Not for commercial use except by explicit permission
' bruce@mcpher.com
' http://sites.mcpher.com/share/Home/excelquirks/proctimer
'
' to get started
' 1.add as addin
' 2.set a reference to cpprofiler in your vbIDE (tools/references)
' 3.run cpprofiler.autoprofileform
'
' -- execute this to get started---------------------
Public Sub autoProfileForm()
   apManageForm.Show
End Sub
'---------------------------------------------------

Public Sub apConstruct(pname As String)
'   so far only set up for 1 profiler
    Const goodName = "apProfiler_1"

    If LCase(pname) <> LCase(goodName) Then
        MsgBox ("Only valid ID implemented so far is " & goodName & " - you provided " & pname)
    Else
        Set apProfiler_1 = New cProcProfiler
    End If
    
 
End Sub
