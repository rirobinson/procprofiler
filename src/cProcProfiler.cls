VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "cProcProfiler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'
' Not for commercial use except by explicit permission
' bruce@mcpher.com
' http://sites.mcpher.com/share/Home/excelquirks/proctimer
'
Public Enum ptLevels
    ptLevelNone = 0
    ptLevelLow
    ptLevelMedium
    ptLevelHigh
    ptLevelAll
End Enum

Private lTimers As Collection
Private lProcTimer As cProcTimer
Private lLevel As ptLevels
Public Property Get Timers() As Collection
    Set Timers = lTimers
End Property
Public Property Get Created() As Date
   Created = lProcTimer.FirstTime
End Property

Public Sub DestroyTimers()
    Dim l As Long
    Dim p As cProcTimer
    For l = lTimers.Count To 1 Step -1
        Set p = lTimers(l)
        lTimers.Remove (l)
        Set p = Nothing
    Next l
    Set lTimers = Nothing
    Set lProcTimer = Nothing
End Sub

Public Sub Start(Section As String, Optional procName As String = "Not given", _
            Optional pLevel As ptLevels = ptLevelLow)
    Dim p As cProcTimer
    Set p = ptExists(lTimers, Section)
    If p Is Nothing Then
        Set p = Create(Section, pLevel, procName)
    End If
    p.Start
    
End Sub
Public Sub Finish(Section As String)
    Dim p As cProcTimer
    Set p = ptExists(lTimers, Section)
    If p Is Nothing Then
        MsgBox ("You are asking to finish a timer section " & Section & " that doesnt exist")
    Else
        p.Finish
    End If
    
End Sub
Public Sub Pause(Section As String)
    Dim p As cProcTimer
    Set p = ptExists(lTimers, Section)
    If p Is Nothing Then
        MsgBox ("You are asking to pause a timer section " & Section & " that doesnt exist")
    Else
        p.Pause
    End If
    
End Sub

Private Function Create(Section As String, Optional pLevel As ptLevels = ptLevelLow, _
        Optional procName As String = "Not given") As cProcTimer
        
    Dim p As cProcTimer
    
        Set p = New cProcTimer
        lTimers.Add p, Section
        With p
            .Level = pLevel
            .Active = (pLevel <= lLevel)
            .procName = procName
            .Name = Section
            .Reset
            Set Create = p
        End With
    

End Function
Public Sub StartProfiler(sname As String, Optional pLevel As ptLevels = ptLevelAll)
    Set lTimers = New Collection
    Set lProcTimer = New cProcTimer
    lLevel = pLevel
    With lProcTimer
        .Level = pLevel
        .Active = (pLevel <> ptLevelNone)
        .Name = sname
        .Reset
        .Start
    End With
    
End Sub
Public Sub FinishProfiler()
    With lProcTimer
        .Finish
    End With
End Sub
Private Function ptExists(co As Collection, v As Variant) As cProcTimer
    
    On Error GoTo handle
    Set ptExists = co(v)
    
    Exit Function
handle:
    Set ptExists = Nothing
    
End Function
Public Sub Results(rOutput As Range)
    Dim r As Range
    If lProcTimer.Active Then
        Set r = procReport(rOutput)
    End If
End Sub
Private Function procReport(rOutput As Range) As Range

    ' results of timing sessions
    Dim p As cProcTimer, ro As Range, t As Variant, lastrow As Range
    Dim n As Long

    t = Array("Section", "Iterations", "Total", "Max", "Min", "Average", "%age", "First", "Last", "Procedure", "Created")
    Set ro = rOutput.Resize(, UBound(t) - LBound(t) + 1)
    If ro.Worksheet.UsedRange.Rows.Count > 0 Then
        ro.Resize(ro.Worksheet.UsedRange.Rows.Count).ClearContents
    End If
    ro.Value = t
    ro.Worksheet.Activate
    n = 0
    For Each p In lTimers
        If p.Active Then
            n = n + 1
        End If
    Next p
    Set lastrow = rOutput.Offset(n + 1)
    For Each p In lTimers
     If p.Active Then
        If p.isOpen Then
           MsgBox ("Section timer " & " was not closed off- cant report")
        Else
           Set ro = ro.Offset(1)
           With p
               ro.Cells(1, 1) = p.Name
               ro.Cells(1, 2) = p.Iterations
               ro.Cells(1, 3) = p.TotalTime
               ro.Cells(1, 4) = p.MaxTime
               ro.Cells(1, 5) = p.MinTime
               ro.Cells(1, 6) = p.MeanTime
               ro.Cells(1, 7).Formula = "=" & ro.Cells(1, 3).Address & "/" & lastrow.Cells(1, 3).Address
               ro.Cells(1, 8) = p.FirstTime
               ro.Cells(1, 9) = p.LastTime
               ro.Cells(1, 10) = p.procName
               ro.Cells(1, 11) = p.Added
           End With
        End If
     End If
    Next p
    
    'totals
    Set lastrow = ro
    If lastrow.Row > rOutput.Row Then
        Set ro = ro.Offset(1)
        With lProcTimer
            ro.Cells(1, 1) = .Name
            ro.Cells(1, 2).Formula = .Iterations
            ro.Cells(1, 3).Formula = .TotalTime
            ro.Cells(1, 4).Formula = "=max(" & rOutput.Cells(1, 4).Address & ":" & lastrow.Cells(1, 4).Address & ")"
            ro.Cells(1, 5).Formula = "=min(" & rOutput.Cells(1, 5).Address & ":" & lastrow.Cells(1, 5).Address & ")"
            ro.Cells(1, 6).Formula = "=average(" & rOutput.Cells(1, 6).Address & ":" & lastrow.Cells(1, 6).Address & ")"
            ro.Cells(1, 8).Formula = "=min(" & rOutput.Cells(1, 8).Address & ":" & lastrow.Cells(1, 8).Address & ")"
            ro.Cells(1, 9).Formula = "=max(" & rOutput.Cells(1, 9).Address & ":" & lastrow.Cells(1, 9).Address & ")"
            ro.Cells(1, 10) = .procName
            ro.Cells(1, 11) = .FirstTime
        End With
    End If
    Set procReport = ro
End Function

