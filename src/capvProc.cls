VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "capvProc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' Not for commercial use except by explicit permission
' bruce@mcpher.com
' http://sites.mcpher.com/share/Home/excelquirks/proctimer
'
' class for process to be autoprofiled

Private lParent As capvModule
Private lName As String
Private lProcKind As vbext_prockind

Private lSelected As Boolean
Public Property Get Selected() As Boolean
    Selected = lSelected
End Property
Public Property Let Selected(m As Boolean)
    lSelected = m
End Property
Public Property Get Name() As String
    Name = lName
End Property
Public Property Get NameandParentandPK() As String
    NameandParentandPK = "(" & lParent.Name & ") " & NameandPK()
End Property
Public Function NameandPK() As String
    NameandPK = pkText() & lName
End Function
Public Property Get Parent() As capvModule
    Set Parent = lParent
End Property
Public Property Get ProcKind() As vbext_prockind
    ProcKind = lProcKind
End Property
Public Sub Init(m As capvModule, pname As String, pk As vbext_prockind)
    Set lParent = m
    lProcKind = pk
    lName = pname
    lSelected = False
    lParent.apvProcs.Add Me, NameandParentandPK()

End Sub
Public Property Get Stub() As String
    Stub = lParent.Parent.Stub
End Property
Public Property Get ResultsAddress() As String
   ResultsAddress = lParent.Parent.ResultsAddress
End Property

Public Function pkText() As String
    Select Case lProcKind
        Case vbext_pk_Get
            pkText = "Get:"
        Case vbext_pk_Set
            pkText = "Set:"
        Case vbext_pk_Let
            pkText = "Let:"
        Case vbext_pk_Proc
            pkText = "Proc:"
        Case Else
            Debug.Assert True
            
    End Select
    
End Function

