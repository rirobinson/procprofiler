VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} apManageForm 
   Caption         =   "Manage AutoProfiling"
   ClientHeight    =   8325
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9840
   OleObjectBlob   =   "apManageForm.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "apManageForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim coVj As Collection, selectedVj As capvProj

Private Sub cbAddAutoProfile_Click()
    
    If oktoGo() Then
        If selectedVj.Manager Is Nothing Then
            MsgBox ("A manager module is mandatory- please select one")
        
        Else

          If Len(reResultsRange.Text) > 0 Then
            Set selectedVj.ResultsRange = Range(reResultsRange.Text)
            Call apProfileModules(selectedVj)
          Else
            MsgBox ("Please select an output cell to report results")
          End If
          
        End If
    End If
End Sub

Private Sub cbExitForm_Click()
    Unload Me
End Sub
Private Function selectedProject() As capvProj
    Dim vj As capvProj
    If isinCollection(coVj, cbProject.Text) Then
        Set vj = coVj(cbProject.Text)
    Else
        Set vj = Nothing
    End If
    Set selectedProject = vj
End Function

Private Sub cbManager_Change()
    If oktoGo() Then
        Set selectedVj.Manager = findwithNameModulePk(cbManager, selectedVj)
    End If
End Sub

Private Sub cbProject_Change()

    Dim vm As capvModule, vp As capvProc
    Dim co As Collection
    
    lbProcUniverse.Clear
    lbProcSelected.Clear
    cbManager.Clear
    
   ' get selected project
    If Not selectedVj Is Nothing Then
     selectedVj.Destroy
    End If
    Set selectedVj = selectedProject()
    tbAutoProfilerID = selectedVj.Stub
    
    ' reset module list for this project
    
    Call apGetmoduleList(selectedVj)
    For Each vm In selectedVj.apvModules
        Call apGetProcList(vm)
    Next vm
    
    ' analyze current profiling to try to recreate selections
    Call apGetProcsWithProfiling(selectedVj)
    If Not selectedVj.ResultsRange Is Nothing Then
        reResultsRange.Text = selectedVj.ResultsAddress
    End If
    '
    ' populate module/proc list
    

    For Each vm In selectedVj.apvModules
        For Each vp In vm.apvProcs
            With lbProcUniverse
                lbProcUniverse.AddItem vp.NameandParentandPK
                If vp.Selected Then
                    .Selected(.ListCount - 1) = True
                End If
            End With
            cbManager.AddItem vp.NameandParentandPK
        Next vp
    Next vm
    
    Call moveList(lbProcUniverse, lbProcSelected, True)
    
    If Not selectedVj.Manager Is Nothing Then
        cbManager = selectedVj.Manager.NameandParentandPK
    End If

End Sub
Private Function oktoGo() As Boolean
    
    oktoGo = True
    If selectedVj Is Nothing Then
        oktoGo = False
    End If
    
    If oktoGo And selectedVj.apvModules.Count = 0 Then
        MsgBox ("Nothing to do - please select a project with some modules")
        oktoGo = False
    End If
    
End Function
Private Sub cbRemoveAutoProfile_Click()
    If oktoGo() Then
        Call apRemoveProfiling(selectedVj)
        Call cbProject_Change
    End If
End Sub

Private Sub cbSelect_Click()
     Call moveList(lbProcUniverse, lbProcSelected, True)
End Sub

Private Sub cbUnselect_Click()
     Call moveList(lbProcSelected, lbProcUniverse, False)
End Sub




Private Sub lbProcSelected_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
 Call cbUnselect_Click
End Sub

Private Sub lbProcUniverse_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    Call cbSelect_Click
End Sub

Private Sub UserForm_Activate()

    Dim vj As capvProj, vm As capvModule, vp As capvProc
    
    ' fill the project combo box
    Set coVj = apgetProjects()
    For Each vj In coVj
        cbProject.AddItem vj.NameandParent
        If vj.wBook.Name = ActiveWorkbook.Name Then
            cbProject.Text = vj.NameandParent
        End If
    Next vj
    
End Sub

Private Sub moveList(lbFrom As MSForms.ListBox, lbTo As MSForms.ListBox, bSelecting As Boolean)
        Dim i As Long, vp As capvProc
        With lbFrom
            If .ListIndex = -1 Then Exit Sub
            For i = .ListCount - 1 To 0 Step -1
                If .Selected(i) Then
                    Set vp = findwithNameModulePk(.List(i), selectedVj)
                    vp.Selected = bSelecting
                    lbTo.AddItem .List(i)
                    .RemoveItem i
                End If
            Next i
        End With
End Sub

