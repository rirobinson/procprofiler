VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "capvProj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Private lWb As Workbook
Private lResultsRange As Range
Private lID As Long
Private lManager As capvProc
Private lapvModules As Collection
Private Const apStub = "apProfiler"
Public Property Get NameandParent() As String
    NameandParent = "(" & lWb.Name & ") " & lWb.VBProject.Name
End Property
Public Property Get wBook() As Workbook
    Set wBook = lWb
End Property
Public Property Get ResultsRange() As Range
    Set ResultsRange = lResultsRange
End Property
Public Property Get ResultsAddress() As String
    ResultsAddress = "'" & lResultsRange.Worksheet.Name & "'!" & lResultsRange.Address
End Property
Public Property Set ResultsRange(r As Range)
    Set lResultsRange = r
End Property
Public Property Get id() As Long
    id = lID
End Property
Public Property Get Stub() As String
    Stub = apStub & "_" & CStr(id)
End Property
Public Property Let id(i As Long)
    lID = id
End Property
Public Property Get Manager() As capvProc
    Set Manager = lManager
End Property
Public Property Set Manager(vp As capvProc)
    Set lManager = vp
End Property
Public Property Get apvModules() As Collection
    Set apvModules = lapvModules
End Property
Public Sub Init(wb As Workbook)
    Set lWb = wb
    lID = 1
    Set lapvModules = New Collection
End Sub
Public Sub Destroy()
    Dim vm As capvModule
    Dim n As Long
    For n = lapvModules.Count To 1 Step -1
         Set vm = lapvModules(n)
         lapvModules.Remove (n)
         vm.Destroy
    Next n

End Sub

