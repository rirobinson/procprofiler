VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "capvModule"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' Not for commercial use except by explicit permission
' bruce@mcpher.com
' http://sites.mcpher.com/share/Home/excelquirks/proctimer
'
' class for process to be autoprofiled

Private lvCom As VBComponent
Private lapvProcs As Collection
Private lParent As capvProj
Public Property Get Name() As String
    Name = lvCom.Name
End Property
Public Property Get Parent() As capvProj
    Set Parent = lParent
End Property
Public Property Get vCom() As VBComponent
    Set vCom = lvCom
End Property
Public Property Get apvProcs() As Collection
    Set apvProcs = lapvProcs
End Property
Public Sub Init(v As VBComponent, pj As capvProj)
    Set lapvProcs = New Collection
    Set lParent = pj
    Set lvCom = v
End Sub

Public Sub Destroy()
    
    Dim n As Long
    For n = lapvProcs.Count To 1 Step -1
         lapvProcs.Remove (n)
    Next n
    
End Sub
