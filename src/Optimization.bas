Attribute VB_Name = "Optimization"
Option Explicit
' Acknowledgement for the microtimer procedures used here to
' thanks to Charles Wheeler - http://www.decisionmodels.com/
Private Declare Function getFrequency Lib "kernel32" _
Alias "QueryPerformanceFrequency" (cyFrequency As Currency) As Long
Private Declare Function getTickCount Lib "kernel32" _
Alias "QueryPerformanceCounter" (cyTickCount As Currency) As Long


Sub timeonesheet()
    Call timeloopSheets(Worksheets("LIsts"))
End Sub
Sub timeallsheets(Optional rOutput As Range = Nothing)
   
   Dim r As Range
   
   If rOutput Is Nothing Then
    Set r = Range("Sheet4!a1")
   Else
    Set r = rOutput
   End If
    
    Call procProfilerConstruct

    Call timeloopSheets
    
    procProfiler.FinishProfiler
    
    Call procProfiler.Results(r)
    
    Call procProfilerDestroy
    
End Sub
Sub timeloopSheets(Optional wsingle As Worksheet)
    
    Dim ws As Worksheet, ro As Range, rAll As Range
    Dim rKey As Range, r As Range, rSum As Range
    Const Where = "ExecutionTimes!a1"
    
    Set ro = Range(Where)
    ro.Worksheet.Cells.ClearContents
    Set rAll = ro
    'headers
    rAll.Cells(1, 1).Value = "address"
    rAll.Cells(1, 2).Value = "time"
    
    If wsingle Is Nothing Then
    ' all sheets
        For Each ws In Worksheets
            Set ro = timeSheet(ws, ro)
        Next ws
    Else
    ' or just a single one
        Set ro = timeSheet(wsingle, ro)
    End If
    
    'now sort results, if there are any

    Call procProfiler.Start("sort", "timeloopsheets")
    If ro.Row > rAll.Row Then
        Set rAll = rAll.Resize(ro.Row - rAll.Row + 1, 2)
        Set rKey = rAll.Offset(1, 1).Resize(rAll.Rows.Count - 1, 1)
        ' sort highest to lowest execution time
        With rAll.Worksheet.Sort
            .SortFields.Clear
            .SortFields.Add Key:=rKey, _
            SortOn:=xlSortOnValues, Order:=xlDescending, _
                DataOption:=xlSortNormal
    
            .SetRange rAll
            .header = xlYes
            .MatchCase = False
            .Orientation = xlTopToBottom
            .SortMethod = xlPinYin
            .Apply
        End With
        ' sum times
        Set rSum = rAll.Cells(1, 3)
        rSum.Formula = "=sum(" & rKey.Address & ")"
        ' %ages formulas
        For Each r In rKey.Cells
            Call procProfiler.Start("Set cell formats", "timeloopsheets")
                r.Offset(, 1).Formula = "=" & r.Address & "/" & rSum.Address
                r.Offset(, 1).NumberFormat = "0.00%"
            Call procProfiler.Finish("Set cell formats")
        Next r
        
    End If
    Call procProfiler.Finish("sort")
    rAll.Worksheet.Activate

End Sub
Function timeSheet(ws As Worksheet, rOutput As Range) As Range
    
    Dim ro As Range
    Dim c As Range, ct As Range, rt As Range, u As Range


    ws.Activate
    Set u = ws.UsedRange
    Set ct = u.Resize(1)
    Call procProfiler.Start("singlesheet", "timesheet")
    Set ro = rOutput
    For Each c In ct.Columns
        Set ro = ro.Offset(1)
        Set rt = c.Resize(u.Rows.Count)
        rt.Select
        ro.Cells(1, 1).Value = rt.Worksheet.Name & "!" & rt.Address
        ro.Cells(1, 2) = shortCalcTimer(rt, False)
    Next c
    Call procProfiler.Finish("singlesheet")
    Set timeSheet = ro

    
End Function


Function shortCalcTimer(rt As Range, Optional bReport As Boolean = True) As Double
    Dim dtime As Double
    Dim sCalcType As String
    Dim lCalcSave As Long
    Dim bIterSave As Boolean
    '
    On Error GoTo Errhandl


    ' Save calculation settings.
    lCalcSave = Application.Calculation
    bIterSave = Application.Iteration
    If Application.Calculation <> xlCalculationManual Then
        Application.Calculation = xlCalculationManual
    End If

    ' Switch off iteration.
    If Application.Iteration <> False Then
        Application.Iteration = False
    End If

' Get start time.
    dtime = MicroTimer
    If val(Application.Version) >= 12 Then
        rt.CalculateRowMajorOrder
    Else
        rt.Calculate
    End If


' Calc duration.
    sCalcType = "Calculate " & CStr(rt.Count) & _
        " Cell(s) in Selected Range: " & rt.Address
    dtime = MicroTimer - dtime
    On Error GoTo 0

    dtime = Round(dtime, 5)
    If bReport Then
        MsgBox sCalcType & " " & CStr(dtime) & " Seconds"
    End If

    shortCalcTimer = dtime
Finish:

    ' Restore calculation settings.
    If Application.Calculation <> lCalcSave Then
         Application.Calculation = lCalcSave
    End If
    If Application.Iteration <> bIterSave Then
         Application.Calculation = bIterSave
    End If
    Exit Function
Errhandl:
    On Error GoTo 0
    MsgBox "Unable to Calculate " & sCalcType, _
        vbOKOnly + vbCritical, "CalcTimer"
    GoTo Finish
End Function
'
Function MicroTimer() As Double
'

' Returns seconds.
'
    Dim cyTicks1 As Currency
    Static cyFrequency As Currency
    '
    MicroTimer = 0

' Get frequency.
    If cyFrequency = 0 Then getFrequency cyFrequency

' Get ticks.
    getTickCount cyTicks1

' Seconds
    If cyFrequency Then MicroTimer = cyTicks1 / cyFrequency
End Function

Sub RangeTimer()
    Call DoCalcTimer(1)
End Sub
Sub SheetTimer()
    Call DoCalcTimer(2)
End Sub
Sub RecalcTimer()
    Call DoCalcTimer(3)
End Sub
Sub FullcalcTimer()
    Call DoCalcTimer(4)
End Sub

Function DoCalcTimer(jMethod As Long, Optional bReport As Boolean = True) As Double
    Dim dtime As Double
    Dim dOvhd As Double
    Dim oRng As Range
    Dim oCell As Range
    Dim oArrRange As Range
    Dim sCalcType As String
    Dim lCalcSave As Long
    Dim bIterSave As Boolean
    Dim s As String
    '
    On Error GoTo Errhandl

' Initialize
    dtime = MicroTimer

    ' Save calculation settings.
    lCalcSave = Application.Calculation
    bIterSave = Application.Iteration
    If Application.Calculation <> xlCalculationManual Then
        Application.Calculation = xlCalculationManual
    End If
    Select Case jMethod
    Case 1

        ' Switch off iteration.

        If Application.Iteration <> False Then
            Application.Iteration = False
        End If
        
        ' Max is used range.

        If Selection.Count > 1000 Then
            Set oRng = Intersect(Selection, Selection.Parent.UsedRange)
        Else
            Set oRng = Selection
        End If

        ' Include array cells outside selection.

        For Each oCell In oRng
            If oCell.HasArray Then
                If oArrRange Is Nothing Then
                    Set oArrRange = oCell.CurrentArray
                End If
                If Intersect(oCell, oArrRange) Is Nothing Then
                    Set oArrRange = oCell.CurrentArray
                    Set oRng = Union(oRng, oArrRange)
                End If
            End If
        Next oCell

        sCalcType = "Calculate " & CStr(oRng.Count) & _
            " Cell(s) in Selected Range: "
    Case 2
        sCalcType = "Recalculate Sheet " & ActiveSheet.Name & ": "
    Case 3
        sCalcType = "Recalculate open workbooks: "
    Case 4
        sCalcType = "Full Calculate open workbooks: "
    End Select

' Get start time.
    dtime = MicroTimer
    Select Case jMethod
    Case 1
        If val(Application.Version) >= 12 Then
            oRng.CalculateRowMajorOrder
        Else
            oRng.Calculate
        End If
    Case 2
        ActiveSheet.Calculate
    Case 3
        Application.Calculate
    Case 4
        Application.CalculateFull
    End Select

' Calc duration.
    dtime = MicroTimer - dtime
    On Error GoTo 0

    dtime = Round(dtime, 5)
    s = sCalcType & " " & CStr(dtime) & " Seconds"
    If bReport Then
        MsgBox s
    End If
    DoCalcTimer = dtime

Finish:

    ' Restore calculation settings.
    If Application.Calculation <> lCalcSave Then
         Application.Calculation = lCalcSave
    End If
    If Application.Iteration <> bIterSave Then
         Application.Calculation = bIterSave
    End If
    Exit Function
Errhandl:
    On Error GoTo 0
    MsgBox "Unable to Calculate " & sCalcType, _
        vbOKOnly + vbCritical, "CalcTimer"
    GoTo Finish
End Function

Public Function newProcTimer() As cProcTimer
   Set newProcTimer = New cProcTimer

End Function
Public Function newProcProfiler() As cProcProfiler
   Set newProcTimer = New cProcTimer

End Function

