Attribute VB_Name = "procProfileWrapper"
Option Explicit
'
' Not for commercial use except by explicit permission
' bruce@mcpher.com
' http://sites.mcpher.com/share/Home/excelquirks/proctimer
'
Public procProfiler As cProcProfiler


Sub procProfilerConstruct(Optional sname As String = "default profiler", _
    Optional pLevel As ptLevels = ptLevelNone)

    Call procProfilerDestroy
    Set procProfiler = New cProcProfiler
    Call procProfiler.StartProfiler(sname, pLevel)
    
End Sub
Sub procProfilerDestroy()
        Call procProfilerDestroyClean(procProfiler)
End Sub
Sub procProfilerDestroyClean(cp As cProcProfiler)

    If Not cp Is Nothing Then
        cp.DestroyTimers
        Set cp = Nothing
    End If
End Sub

Sub exampleProfile()
    Dim i As Long
    ' create defaultprofiler, and start
    Call procProfilerConstruct(, ptLevelAll)
    '
    ' ... some code
    '
    Call procProfiler.Start("Manager", "example Profile", ptLevelLow)
    '
    ' ... some code
    '
    ' pause timing to collect user input
    '
    Call procProfiler.Pause("Manager")
    '
    MsgBox ("restart timing")
    '
    Call procProfiler.Start("Manager")
    '
    ' some more code
    '
    For i = 1 To 100
        Call exampleprofilesub
    Next i
    '
    ' some more code
    '
    Call procProfiler.Finish("Manager")
    '
    ' all over
    '
    Call procProfiler.FinishProfiler
    '
    ' report results
    '
    Call procProfiler.Results(Range("ResultSheet!a1"))
    '
    ' clean
    '
    Call procProfilerDestroy
    '
End Sub
Sub exampleprofilesub()
'
' some more code
'
    Dim i As Long, d As Double
    Call procProfiler.Start("Sub", "exampleprofilesub", ptLevelMedium)
    '
    'some more code
    For i = 1 To 1000
        Call procProfiler.Start("Random loop", "exampleprofilesub", ptLevelHigh)
        d = Sqr(Rnd(i)) * Sqr(Rnd())
        Call procProfiler.Finish("Random loop")
    Next i
    '
    ' some more code
    '
    Call procProfiler.Finish("Sub")
End Sub
 
