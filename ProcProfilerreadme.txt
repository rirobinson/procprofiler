Thank you for downloading this Excel profiler, which comes as an excel plugin (.xlam).


There are various useful modules included such as 

- Optimization. Call timeallsheets() to provide a report on how long each column in your workbook takes to execute.
- ProcProfilerWrapper. A set of tools to profile your VBA procedures.
- ApPublic. A set of tools to automatically insert and remove profiling hooks into your procedures. Call autoprofileform() to kick off
- Various utilities and classes to quickly build your own profiling, either one off or permanently. 
- Optimize.bas is also provided as a separate file if you only want to optimize spreadsheet (not VBA code) without using the add-in


Who Needs this ?

Anyone who wants to investigate slow worksheet performance, any developer who wants to identify poor VBA performance, 
or any developer who wants to build in permament performance profiling or rogue case identification to their application.

Terms of use.

' Not for commercial use except by explicit permission
' full documentation available 
' http://sites.mcpher.com/share/

Please provide any feedback, questions, commments to 
' bruce@mcpher.com

Download file
http://sites.mcpher.com/share/Home/excelquirks/downloads/procProfiler.zip

' Acknowledgement for the microtimer procedures used here to
' thanks to Charles Wheeler - http://www.decisionmodels.com/
